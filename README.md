# Trust Tools

Rebuilding trust between consumers and brands.

Our cross-disciplinary team is working to design and develop tools which will redress the power imbalance which is implicit in long-winded terms and conditions, empowering customers to make informed decisions regarding their personal data, and enabling brands to demonstrate their commitment to trustworthiness and openness.
Current work is focussed on a browser extension which will parse terms and conditions, compare them to user preferences, and flag any deviations.

## MozFest

An illustratory mockup is available [here](https://tedarbyshire.github.io/trust-tools/tools/pp-generator/mozfest-mockup/mozfest-mockup.html)

## Roadmap

Which milestones are we working towards at the moment?
If you see an opportunity to get involved, email us or get in touch via github.

### Milestone: Collaborate on decision tree modelling

Date: 26/09

This week we are working on finalising the design for the decision tree a consumer will interact with when setting their preferences.
The work is based on a document sent out to nPower customers entitled 'using your personal information - our fair processing (privacy) notice'.
This is available as a PDF on the nPower website.

### Milestone: Release white paper

The white paper laying the groundwork for the upcoming tool development is available
[from this repository](papers/better-than-compliant.pdf).

### Milestone: Prepare for upcoming events

Date: MozFest 26/10

We are working towards having a completed model before events.
At these we will test the value of the tool from both a brand and a user perspective.

### Milestone: Compile research

Date: 09/11

We will compile the brand and consumer research and release a paper containing the results.
This information will be used in the design of the extension.

### Milestone: Tool design and development

Date: Ongoing

We will continue to prepare our tool for release.

## Areas we need help in

Please get in touch if:

- You have NLP expertise and would like to help us develop a strategy and framework for parsing the legal text
- You have legal expertise and would like to support our strategy for engaging with legislation
- You're keen to get involved in any other way :)